//AXA Lock Tester to check if the lock or cable is working correctly

const int state_in = 12;  //PIN ASSIGNMENT
const int out_a = 2;      
const int out_b = 3;

int lectura;
int estado;

void abrircandado(){
    Serial.println("Abriendo candado");
    digitalWrite(out_a, LOW);
    delay (100);
    digitalWrite(out_b, HIGH);
    delay (100);
}

void cerrarcandado() {
    Serial.println("Cerrando candado");
    digitalWrite(out_a, HIGH);
    delay (100);
    digitalWrite(out_b, LOW);
    delay (100);
}

void estadopines() {
    Serial.println("OUT A");
    if (digitalRead (out_a)) Serial.println ("1") ;
    if (!digitalRead (out_a)) Serial.println ("0") ;
    Serial.println("OUT B");
    if (digitalRead (out_b)) Serial.println ("1") ;
    if (!digitalRead (out_b)) Serial.println ("0") ;
}

void setup () {
    Serial.begin(9600);
    pinMode (state_in, INPUT);
    pinMode (out_a, OUTPUT);
    pinMode (out_b, OUTPUT);
    digitalWrite(out_a, LOW);   //INITIALIZE WITH OUT_A and OUT_B WITH 0
    digitalWrite(out_b, LOW);
    Serial.println("AXA Lock Test v1");
    Serial.println("El estado del candado es:");
    estado = digitalRead (state_in);
    Serial.println(estado);
    if (!digitalRead (state_in)) Serial.println ("Candado abierto") ;
    if (digitalRead (state_in)) Serial.println ("Candado cerrado") ;
    Serial.println("Estado puertos de salida:");
    estadopines();
    delay (4000);
    Serial.println("Introduce: 0 = abrir / 1 = cerrar");  
    }

void loop() {
    
    if(Serial.available() > 0) {      
        lectura=Serial.parseInt();
        Serial.println("La lectura es");
        Serial.println(lectura);
        delay (1000);
        if (lectura == 0) (abrircandado());
        if (lectura == 1) (cerrarcandado());
        estadopines();
        Serial.println("Introduce: 0 = abrir / 1 = cerrar");
        delay (5000);
        Serial.println("El estado del candado es:");
        estado = digitalRead (state_in);
        Serial.println(estado);
        if (!digitalRead (state_in)) Serial.println ("Candado abierto") ;
        if (digitalRead (state_in)) Serial.println ("Candado cerrado") ;
        while (Serial.available() >0 ) {    // CLEARS SERIAL BUFFER
            Serial.read();                      //
        }
    }
}